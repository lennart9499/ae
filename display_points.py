#!/usr/bin/python
import matplotlib.pyplot as plt
import matplotlib.patches as mpatch
import sys
import math

# altered version of display.py

fig, ax = plt.subplots()

points = {}

range_x = [0, 0]
range_y = [0, 0]

if len(sys.argv) > 1:
    with open(sys.argv[1], "r") as f:
        for x in f.readlines()[1:]:
            data = x.split()
            x, y = int(data[0]), int(data[1])
            # false is legacy from original script
            points[data[4]] = (False, x, y)
            range_x[0] = min(range_x[0], x)
            range_x[1] = max(range_x[1], x)
            range_y[0] = min(range_y[0], y)
            range_y[1] = max(range_y[1], y)

    for i, p in enumerate(points):
        c = "green"
        plt.plot([points[p][1]], [points[p][2]], marker='o', markersize=5, color=c)
        #circle = plt.Circle((points[p][1], points[p][2]), radius=0.1, color=c, zorder=20)
        #ax.add_patch(circle)
       # ax.annotate(str(i + 1), xy=(points[p][1], points[p][2]), zorder=30, fontsize=8, color='w', weight='bold',
       #            ha="center", va="center")
    
    width = range_x[1] - range_x[0]
    width_buf = math.ceil(0.05 * width)
    left = math.floor(range_x[0] - width_buf)
    right = math.ceil(range_x[1] + width_buf)
    heigth = range_y[1] - range_y[0]
    heigth_buf = math.ceil(0.05 * heigth)
    down = math.floor(range_y[0] - heigth_buf)
    up = math.ceil(range_y[1] + heigth_buf)
    ax.set_xlim((left, right))
    ax.set_ylim((down, up))
    ax.set_aspect('equal')

    #plt.axis('off')
    #plt.xticks(range(left, right))
    #plt.yticks(range(down, up))

    #plt.axhline(linewidth=2, color='gray')  # adds thick red line @ y=0
    #plt.axvline(linewidth=2, color='gray')  # adds thick red line @ x=0

    #plt.grid(b=True, which='major', color='#666666', linestyle='-')

    plt.show()
