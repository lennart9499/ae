//
// Created by lennart on 28.10.2020.
//

#include "OptimalSolver.h"
#include <tuple>

// fac?
int OptimalSolver::fac(int vector_pos, std::vector<Point> &points, int &points_with_label_t, int legal_points,
        std::vector<Point> &points_best) {
    //Rekursionsanker
    if (vector_pos == points.size()) {
        if (points_with_label_t < legal_points) {
            points_with_label_t = legal_points;
            points_best = std::vector<Point>(points);
        }
        return 0;
    }

    //Rekursive Aufrufe mit den 5 Möglichkeiten, 1x nicht plazieren, 4x an einer Ecke platzieren
    for (int i = 0; i < 5; i++) {
        if (points_with_label_t == points.size()) {
            //Done if only one optimal solution should be found, because every label is placed
            return 0;
        } else if (i == 4) {
            points[vector_pos].g_solution_x = 0;
            points[vector_pos].g_solution_y = 0;
            points[vector_pos].g_exists = false;
            fac(vector_pos + 1, points, points_with_label_t, legal_points, points_best);
        } else {
            points[vector_pos].g_exists = true;
            points[vector_pos].g_solution_x = std::get<0>(points[vector_pos].getPossibleLabelPositions()[i]);
            points[vector_pos].g_solution_y = std::get<1>(points[vector_pos].getPossibleLabelPositions()[i]);
            if (Point::checkOverlap(points[vector_pos], points)) {
                //Wenn sich hier schon ein Punkt überlappt kann am Ende keine legale Lösung entstehen
                continue;
            }
            fac(vector_pos + 1, points, points_with_label_t, legal_points + 1, points_best);
        }
    }
    return 1;
}


std::vector<Point> OptimalSolver::solve(std::vector<Point> &points) {
    std::vector<Point> points_best;
    int points_with_label_t = 0;

    //alle exists auf false setzen
    for (Point &point : points) {
        point.g_exists = false;
        point.g_solution_x = 0;
        point.g_solution_y = 0;
    }

    //Rekursion starten
    fac(0, points, points_with_label_t, 0, points_best);

    return points_best;
}
