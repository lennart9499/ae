#ifndef AE_QTPOINT_H
#define AE_QTPOINT_H

#include <string>
#include <tuple>
#include <utility>
#include <vector>

#include "Point.h"

// QtPoint is basically a dumber 'Point' since some of the Point functionality was moved to 'Instance'.
// Assumption: we have a 'normal' coordinate system with the y-axis pointing to the north.
class QtPoint {
   public:
    unsigned long long g_id;

    int g_x, g_y;
    int g_width, g_height;
    std::string g_name;

    bool g_exists;

    int g_solution_x, g_solution_y;
    
    // TODO: merge constructors
    QtPoint(unsigned long long id, int x, int y, int l, int h, std::string t, bool b_, int x_, int y_) {
        g_id = id;

        g_x = x;
        g_y = y;

        g_height = h;
        g_width = l;

        g_name = std::move(t);

        g_exists = b_;
        g_solution_x = x_;
        g_solution_y = y_;
    }
    QtPoint(const QtPoint& p) {
        g_id = p.g_id;

        g_x = p.g_x;
        g_y = p.g_y;

        g_height = p.g_height;
        g_width = p.g_width;

        g_name = p.g_name;

        g_exists = p.g_exists;
        g_solution_x = p.g_solution_x;
        g_solution_y = p.g_solution_y;
    }
    QtPoint(const Point& p) {
        g_id = p.g_id;

        g_x = p.g_x;
        g_y = p.g_y;

        g_height = p.g_height;
        g_width = p.g_width;

        g_name = p.g_name;

        g_exists = p.g_exists;
        g_solution_x = p.g_solution_x;
        g_solution_y = p.g_solution_y;
    }
    
    // Returns a normal 'Point' with same values.
    Point to_point() {
        return Point(g_id, g_x, g_y, g_width, g_height, g_name, g_exists, g_solution_x, g_solution_y);
    }
    // Returns a tuple of all four possible label positions.
    std::vector<std::tuple<int, int>> getPossibleLabelPositions();

    // Returns true if both points labels overlap
    bool checkOverlap(QtPoint const& p);

    // Compares two points by their label area. Can be used with std::sort.
    static bool compareLabelArea(QtPoint& a, QtPoint& b) { return a.g_width * a.g_height < b.g_width * b.g_height; }

    // overloaded operators
    bool operator==(QtPoint const& point) const { return point.g_id == g_id; }
    bool operator!=(QtPoint const& point) const { return point.g_id != g_id; }
};

#endif  //AE_QTPOINT_H
