//
// Created by lennart on 28.10.2020.
//

#ifndef AE_BASESOLVER_H
#define AE_BASESOLVER_H


#include <vector>
#include "src/util/Point.h"

class BaseSolver {
public:
    virtual std::vector<Point> solve(std::vector<Point> &points) {
        std::cerr << "Base shouldn't do anything!" << std::endl;
        return points;
    }

};


#endif //AE_BASESOLVER_H
