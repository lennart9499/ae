//
// Created by lennart on 28.10.2020.
//

#include "BetterBasicSolver.h"

std::vector<Point> BetterBasicSolver::solve(std::vector<Point>& points)  {
    //alle exists auf false setzen
    for (Point &point : points) {
        point.g_exists = false;
        point.g_solution_x = 0;
        point.g_solution_y = 0;
    }

    //alle exists auf false setzen
    bool problem;
    for (std::vector<Point>::size_type i = 0; i < points.size(); i++) {
        Point &point = points[i];

        //check all possible label positions:
        for(int c = 0; c < 4; c++) {
            point.g_exists = true;
            point.g_solution_x = std::get<0>(point.getPossibleLabelPositions()[c]);
            point.g_solution_y = std::get<1>(point.getPossibleLabelPositions()[c]);

            problem = false;
            for (std::vector<Point>::size_type j = 0; j < points.size(); j++) {
                Point &other = points[j];
                if (other.g_exists && i != j && Point::checkOverlap(point, other)) {
                    problem = true;
                    break;
                }
            }
            if (problem) {
                point.g_exists = false;
                point.g_solution_x = 0;
                point.g_solution_y = 0;
            }else{
                break;
            }
        }
    }

    return points;
}
