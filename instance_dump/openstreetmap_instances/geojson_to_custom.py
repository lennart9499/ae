#!/usr/bin/python

import sys
import json
import math
import random
import os
#CHAR_FACTOR = 10
#########################
# https://gist.github.com/johnberroa/cd49976220933a2c881e89b69699f2f7
def remove_umlaut(string):
    """
    Removes umlauts from strings and replaces them with the letter+e convention
    :param string: string to remove umlauts from
    :return: unumlauted string
    """
    u = 'ü'.encode()
    U = 'Ü'.encode()
    a = 'ä'.encode()
    A = 'Ä'.encode()
    o = 'ö'.encode()
    O = 'Ö'.encode()
    ss = 'ß'.encode()

    string = string.encode()
    string = string.replace(u, b'ue')
    string = string.replace(U, b'Ue')
    string = string.replace(a, b'ae')
    string = string.replace(A, b'Ae')
    string = string.replace(o, b'oe')
    string = string.replace(O, b'Oe')
    string = string.replace(ss, b'ss')

    string = string.decode('utf-8')
    return string

#########################
# https://wiki.openstreetmap.org/wiki/Mercator
# Python Implementation by Paulo Silva
def merc_x(lon):
  r_major=6378137.000
  return r_major*math.radians(lon)

def merc_y(lat):
  if lat>89.5:lat=89.5
  if lat<-89.5:lat=-89.5
  r_major=6378137.000
  r_minor=6356752.3142
  temp=r_minor/r_major
  eccent=math.sqrt(1-temp**2)
  phi=math.radians(lat)
  sinphi=math.sin(phi)
  con=eccent*sinphi
  com=eccent/2
  con=((1.0-con)/(1.0+con))**com
  ts=math.tan((math.pi/2-phi)/2)/con
  y=0-r_major*math.log(ts)
  return y
#########################



def convert(file_path):
    result = []
    min_x = math.inf
    max_x = -math.inf
    min_y = math.inf
    max_y = -math.inf

    with open(file_path) as file:
        data = json.load(file)
        for feature in data["features"]:
            properties = feature["properties"]
            if "name" in properties:
                t = properties["name"]
                t = remove_umlaut(t).strip().replace(" ", "_").replace("\n","")
                coords = feature["geometry"]["coordinates"]
                x = merc_x(coords[0])
                y = merc_y(coords[1])
                l = len(t) * (10 + random.randint(0,20))
                h = 20 + random.randint(0,20)
                entry = [x, y, l, h, t, 0, 0, 0]
                result.append(entry)
                
                if x <= min_x:
                    min_x = x
                if x >= max_x:
                    max_x = x
                if y <= min_y:
                    min_y = y
                if y >= max_y:
                    max_y = y
        
        width = max_x - min_x
        heigth = max_y - min_y
        scale_x = 2000.0 / width
        scale_y = 2000.0 / heigth
        
        for i in range(len(result)):
            entry = result[i]
            entry[0] = round((entry[0] - min_x) * scale_x)
            entry[1] = round((entry[1] - min_y) * scale_y)
            result[i] = "\t".join(str(o) for o in entry) + "\n"
        

        output_path = "instances/geo/" + os.path.basename(file_path).replace("geojson", "txt")
        with open(output_path, 'w') as output_file:
            output_file.write(str(len(result)) + "\n")
            output_file.writelines(result)


# https://stackoverflow.com/questions/2909975/python-list-directory-subdirectory-and-files
for path, subdirs, files in os.walk("."):
    for name in files:
        if name.endswith(".geojson"):
            print("Converting " + os.path.join(path, name) + " ...")
            convert(os.path.join(path,name))

print("Done")