//
// Created by lennart on 28.10.2020.
//

#ifndef AE_BETTERBASICSOLVER_H
#define AE_BETTERBASICSOLVER_H


#include <vector>
#include <src/util/Point.h>
#include "BaseSolver.h"
#include <tuple>

class BetterBasicSolver : public BaseSolver {
public:
    std::vector<Point> solve(std::vector<Point> &points) override;
};


#endif //AE_BASICSOLVER_H
