#include "Quadtree.h"

bool Quadtree::insert(QtPoint& p, const std::tuple<int, int>& label_pos) {
    std::vector<Quadtree*> insert_candidates;
    bool result = check(p, label_pos, insert_candidates);

    if (result) {
        if (p.g_exists) remove(p);
        p.g_solution_x = std::get<0>(label_pos);
        p.g_solution_y = std::get<1>(label_pos);
        p.g_exists = true;
        for (Quadtree* t : insert_candidates) {
            t->append(p);
        }
    }
    return result;
}

bool Quadtree::check(QtPoint& p, const std::tuple<int, int>& label_pos, std::vector<Quadtree*>& insert_candidates) {
    std::tuple<int, int> old_label_pos(p.g_solution_x, p.g_solution_y);
    p.g_solution_x = std::get<0>(label_pos);
    p.g_solution_y = std::get<1>(label_pos);

    bool result = check(p, insert_candidates);

    p.g_solution_x = std::get<0>(old_label_pos);
    p.g_solution_y = std::get<1>(old_label_pos);

    return result;
}

bool Quadtree::check(QtPoint& p, std::vector<Quadtree*>& insert_candidates) {
    if (is_leaf()) {
        // check for collision
        for (QtPoint* other : g_points) {
            if (p != *other && p.checkOverlap(*other)) return false;
        }
        insert_candidates.push_back(this);
        return true;
    }
    // from here on out the existence of children is guaranteed

    // check recursively
    // get lower and upper values for the label
    int x_l = p.g_solution_x;
    int y_u = p.g_solution_y;
    int x_u = p.g_solution_x + p.g_width;
    int y_l = p.g_solution_y - p.g_height;
    bool result = true;

    // NE
    if (result && aabbOverlap(x_l, y_u, x_u, y_l, g_centerx, g_maxy, g_maxx, g_centery)) result &= g_children[OrdDir::NE]->check(p, insert_candidates);
    // NW
    if (result && aabbOverlap(x_l, y_u, x_u, y_l, g_minx, g_maxy, g_centerx, g_centery)) result &= g_children[OrdDir::NW]->check(p, insert_candidates);
    // SW
    if (result && aabbOverlap(x_l, y_u, x_u, y_l, g_minx, g_centery, g_centerx, g_miny)) result &= g_children[OrdDir::SW]->check(p, insert_candidates);
    // SE
    if (result && aabbOverlap(x_l, y_u, x_u, y_l, g_centerx, g_centery, g_maxx, g_miny)) result &= g_children[OrdDir::SE]->check(p, insert_candidates);

    return result;
}

bool Quadtree::aabbOverlap(int x0, int y0, int x1, int y1, int x2, int y2, int x3, int y3) {
    return x0 <= x3 && x2 <= x1 && y3 <= y0 && y1 <= y2;
}

void Quadtree::append(QtPoint& p) {
    assert(is_leaf());
    // if p already stored return
    if (std::find(g_points.begin(), g_points.end(), &p) != g_points.end()) {
    //if(std::find_if(g_points.begin(), g_points.end(), [&p](const QtPoint* q) { return *q == p; }) != g_points.end()){
        
        printf("Tried to append already stored point!");
        return;
    }

    g_points.push_back(&p);
    // if capacity not exceeded we're done
    if (g_points.size() <= CAPACITY) {
        return;
        // else create children and additionally insert content of g_p
    } else {
        subdivide();
        std::vector<QtPoint*> tmp = g_points;
        g_points = std::vector<QtPoint*>();
        for (QtPoint* tmp_q : tmp) {
            QtPoint& q = *tmp_q;
            int x_l = q.g_solution_x;
            int y_u = q.g_solution_y;
            int x_u = q.g_solution_x + q.g_width;
            int y_l = q.g_solution_y - q.g_height;

            // NE
            if (aabbOverlap(x_l, y_u, x_u, y_l, g_centerx, g_maxy, g_maxx, g_centery)) g_children[OrdDir::NE]->append(q);
            // NW
            if (aabbOverlap(x_l, y_u, x_u, y_l, g_minx, g_maxy, g_centerx, g_centery)) g_children[OrdDir::NW]->append(q);
            // SW
            if (aabbOverlap(x_l, y_u, x_u, y_l, g_minx, g_centery, g_centerx, g_miny)) g_children[OrdDir::SW]->append(q);
            // SE
            if (aabbOverlap(x_l, y_u, x_u, y_l, g_centerx, g_centery, g_maxx, g_miny)) g_children[OrdDir::SE]->append(q);
        }
    }
}

void Quadtree::remove(QtPoint& p) {
    p.g_exists = false;
    
    if (is_leaf()) {
        // check for collision
        auto it = g_points.begin();
        while (it != g_points.end()) {
            if (p == **it) {
                it = g_points.erase(it);
            } else {
                ++it;
            }
        }
        return;
    }

    // from here on out the existence of children is guaranteed

    // remove recursively
    // get lower and upper values for the label
    int x_l = p.g_solution_x;
    int y_u = p.g_solution_y;
    int x_u = p.g_solution_x + p.g_width;
    int y_l = p.g_solution_y - p.g_height;

    // NE
    if (aabbOverlap(x_l, y_u, x_u, y_l, g_centerx, g_maxy, g_maxx, g_centery)) g_children[OrdDir::NE]->remove(p);
    // NW
    if (aabbOverlap(x_l, y_u, x_u, y_l, g_minx, g_maxy, g_centerx, g_centery)) g_children[OrdDir::NW]->remove(p);
    // SW
    if (aabbOverlap(x_l, y_u, x_u, y_l, g_minx, g_centery, g_centerx, g_miny)) g_children[OrdDir::SW]->remove(p);
    // SE
    if (aabbOverlap(x_l, y_u, x_u, y_l, g_centerx, g_centery, g_maxx, g_miny)) g_children[OrdDir::SE]->remove(p);
}

// needed?
bool Quadtree::empty() {
    return is_leaf() && g_points.empty();
}

bool Quadtree::is_leaf() {
    return g_children[0] == nullptr;
}

// Initializes children. Should only be called if is_leaf() == true.
void Quadtree::subdivide() {
    g_children[Quadtree::OrdDir::NE] = std::unique_ptr<Quadtree>(new Quadtree(g_centerx, g_maxx, g_centery, g_maxy));
    g_children[Quadtree::OrdDir::NW] = std::unique_ptr<Quadtree>(new Quadtree(g_minx, g_centerx, g_centery, g_maxy));
    g_children[Quadtree::OrdDir::SW] = std::unique_ptr<Quadtree>(new Quadtree(g_minx, g_centerx, g_miny, g_centery));
    g_children[Quadtree::OrdDir::SE] = std::unique_ptr<Quadtree>(new Quadtree(g_centerx, g_maxx, g_miny, g_centery));
}

void Quadtree::print_tree(bool show_borders) {
    rec_print(0, -1, show_borders);
}

void Quadtree::rec_print(int level, int child_id, bool show_borders) {
    for (int i = 0; i < level; i++) {
        printf(" ");
    }
    printf("%d", child_id);
    if (show_borders) printf(" [x:%d - %d,y:%d - %d]", g_minx, g_maxx, g_miny, g_maxy);

    if (is_leaf()) {
        if (!g_points.empty()) {
            printf(" %ld points\n", g_points.size());

        } else {
            printf(" empty\n");
        }
    } else {
        printf("\n");
        for (int i = 0; i < 4; i++) {
            g_children[i]->rec_print(level + 1, i, show_borders);
        }
    }
}

std::vector<QtPoint*> Quadtree::get_points() {
    if (is_leaf()) {
        for (QtPoint* p : g_points) {
            assert(p->g_exists);
        }
        return g_points;
    }
    std::vector<QtPoint*> res;
    for (int i = 0; i < 4; i++) {
        for (QtPoint* p : g_children[i]->get_points()) {
            if (std::find_if(res.begin(), res.end(), [p](const QtPoint* q) { return *q == *p; }) == res.end()) res.push_back(p);
        }
    }
    return res;
}
