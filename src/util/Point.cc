//
// Created by lennart on 23.10.20.
//

#include <tuple>
#include <regex>
#include "Point.h"

std::vector<Point> Point::parseFile(std::string &file) {
    std::vector<Point> points;

    int x, y, l, h;
    std::string t;

    //solved values
    int b_, x_, y_;

    int lines;

    std::ifstream infile(file);
    if (!infile.is_open()) {
        std::cerr << "ERROR: Input File does not exist!" << std::endl;
        exit(1);
    }

    //check malformed file, necessary?

    std::string fileLine;
    std::regex e_used;
    std::regex e_count("(-?\\d+)\\s?");
    // original regex
    //std::regex e_other(
    //    "\\w?(-?\\d+)\\s+(-?\\d+)\\s+(-?\\d+)\\s+(-?\\d+)\\s+([a-zA-ZöÖäÄüÜß\\-+_\\.:]+)\\s+(-?\\d+)\\s+(-?\\d+)\\s+(-?\\d+)\\s?");
    std::regex e_other(
            "\\w?(-?\\d+)\\s+(-?\\d+)\\s+(-?\\d+)\\s+(-?\\d+)\\s+\\S+\\s?(\\s+(-?\\d+)\\s+(-?\\d+)\\s+(-?\\d+)\\s?)?");
    
    int linenumber = 0;
    int entry_cnt = 0;
    while (std::getline(infile, fileLine)) {
        linenumber++;        
        // continue if the line's empty
        if (!fileLine.compare("\n")){
            continue;
        }
        if (linenumber == 1) {
            e_used = e_count;
        } else {
            e_used = e_other;
            entry_cnt++;
        }
        if (!std::regex_match(fileLine, e_used)) {
            std::cerr << "ERROR: File not correctly formatted. Line: " << linenumber << std::endl;
            exit(1);
        }
    }

    //read file
    infile = std::ifstream(file);
    infile >> lines;
    
    if (lines != entry_cnt) {
        std::cerr << "ERROR: First line (count) does not correspond to the actual count! " << entry_cnt << " vs"
                  << lines << std::endl;
        exit(1);
    }

    //read data
    unsigned long long id = 0;
    while (infile >> x >> y >> l >> h >> t >> b_ >> x_ >> y_) {
        points.emplace_back(id++, x, y, l, h, t, b_, x_, y_);
    }


    return points;
}

bool Point::checkOverlap(Point point, std::vector<Point> &points) {
    for (Point &p : points) {
        if (point != p) {
            if (p.g_exists) {
                if (Point::checkOverlap(point, p)) {
                    return true;
                }
            }
        }
    }
    return false;
}

bool Point::checkOverlap(Point &p1, Point &p2) {
    // If one rectangle is on left side of other
    if (p1.g_solution_x >= (p2.g_solution_x + p2.g_width) || p2.g_solution_x >= (p1.g_solution_x + p1.g_width))
        return false;

    // If one rectangle is above other
    return !(p1.g_solution_y <= (p2.g_solution_y - p2.g_height) || p2.g_solution_y <= (p1.g_solution_y - p1.g_height));
}

bool Point::checkOverlapEverPossible(const Point &p1, const Point &p2){
    // If one rectangle is on left side of other
    if (p1.g_x - p1.g_width >= (p2.g_x + p2.g_width) || p2.g_x - p2.g_width >= (p1.g_x + p1.g_width))
        return false;

    // If one rectangle is above other
    return !(p1.g_y + p1.g_width <= (p2.g_y - p2.g_height) || p2.g_y + p2.g_width <= (p1.g_y - p1.g_height));
}

bool Point::checkOverlapEverPossible(Point point, std::vector<Point> &points) {
    for (Point &p : points) {
        if (point != p) {
            if (Point::checkOverlapEverPossible(point, p)) {
                return true;
            }
        }
    }
    return false;
}

std::vector<std::tuple<int, int>> Point::getPossibleLabelPositions() {
    // Access via std::get<0>(NAME)
    std::vector<std::tuple<int, int>> tuples;
    tuples.emplace_back(std::make_tuple(g_x, g_y));
    tuples.emplace_back(std::make_tuple(g_x - g_width, g_y));
    tuples.emplace_back(std::make_tuple(g_x, g_y + g_height));
    tuples.emplace_back(std::make_tuple(g_x - g_width, g_y + g_height));

    return tuples;
}

void Point::serializeFile(std::string &outFile, std::vector<Point> &points) {
    //write data
    std::ofstream outfile(outFile);
    if (outfile.is_open()) {
        outfile << points.size() << "\n";
        for (Point &point : points) {
            outfile << point.g_x << " " << point.g_y << " " << point.g_width << " " << point.g_height << " "
                    << point.g_name << " " << (int) point.g_exists
                    << " " << point.g_solution_x << " " << point.g_solution_y << "\n";
        }
        outfile.close();
    } else std::cout << "Unable to open file" << std::endl;
}

Point::Feasibility Point::checkFeasibility(std::vector<Point> points, bool verbose, bool onlyErrors) {
    bool cornerfound;
    for (Point point : points) {
        if (point.g_exists) {
            cornerfound = false;
            //std::cout << point.g_name << ":"<< std::endl;
            for (std::tuple<int, int> pt : point.getPossibleLabelPositions()) {
                //std::cout << std::get<0>(pt) << " ** " << std::get<1>(pt) << std::endl;
                if (std::get<0>(pt) == point.g_solution_x && std::get<1>(pt) == point.g_solution_y) {
                    cornerfound = true;
                }
            }
            if (!cornerfound) {
                std::cerr << "ERROR: Label position not at corner! " << point.g_name << std::endl;
                return Point::Feasibility::CORNER_ERROR;
            }
        }
    }

    //Check overlapping
    int points_with_label = 0;
    for (Point &point1 : points) {
        for (Point &point2 : points) {
            if (point1 != point2) {
                if (point1.g_exists && point2.g_exists) {
                    //test if top left edge from point1 is in box from point2
                    //std::cout << point1.g_name << " - " << point2.g_name << std::endl;
                    if (Point::checkOverlap(point1, point2)) {
                        std::cerr << "ERROR: Between label \"" << point1.g_name << "\" and \"" << point2.g_name
                                  << "\"" << std::endl;
                        return Point::Feasibility::OVERLAP_ERROR;
                    }

                }
            }
        }
        if (point1.g_exists) {
            points_with_label++;
        }
    }
    if (!onlyErrors) {
        if (verbose)
            std::cout << "All labels legally placed!" << std::endl;
        std::cout << points_with_label << std::endl;
    }
    return Point::Feasibility::OK;
}
