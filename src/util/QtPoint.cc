#include "QtPoint.h"

std::vector<std::tuple<int, int>> QtPoint::getPossibleLabelPositions() {
    // Access via std::get<0>(NAME)
    std::vector<std::tuple<int, int>> tuples;
    tuples.emplace_back(std::make_tuple(g_x, g_y));
    tuples.emplace_back(std::make_tuple(g_x - g_width, g_y));
    tuples.emplace_back(std::make_tuple(g_x, g_y + g_height));
    tuples.emplace_back(std::make_tuple(g_x - g_width, g_y + g_height));

    return tuples;
}

bool QtPoint::checkOverlap(QtPoint const& p) {
    // If one rectangle is on left side of other
    if (p.g_solution_x >= (g_solution_x + g_width) || g_solution_x >= (p.g_solution_x + p.g_width))
        return false;

    // If one rectangle is above other
    return !(p.g_solution_y <= (g_solution_y - g_height) || g_solution_y <= (p.g_solution_y - p.g_height));
}
