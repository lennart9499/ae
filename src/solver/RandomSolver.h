//
// Created by lennart on 28.10.2020.
//

#ifndef AE_RANDOMSOLVER_H
#define AE_RANDOMSOLVER_H


#include <vector>
#include <src/util/Point.h>
#include "BaseSolver.h"
#include <stdlib.h>
#include <tuple>

class RandomSolver : public BaseSolver {
public:
    std::vector<Point> solve(std::vector<Point> &points) override;
};


#endif //AE_BASICSOLVER_H
