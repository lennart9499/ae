#include "QtGeneticAlgorithm.h"

//change some set of points and change some label positions
// TODO: whole function seems strange (also in orignal) -> ask tim
void mutation(Instance& inst, int iteration) {
    std::srand(std::time(0) + iteration * 27);

    int range = inst.g_points.size() * 0.2;

    int undoPoints = (rand() % range) + 1;

    std::vector<int> indices(undoPoints);

    //deleteLabel for random number of points
    for (int i = 0; i < undoPoints; i++) {
        int pointToUndo = (rand() % inst.g_points.size()) + 1;
        indices[i] = pointToUndo;
        inst.remove(inst.g_points[pointToUndo]);
    }
    //look if other points can be set
    // TODO: strange. similiar behaviour to original, but is it a bug?
    for (int k = 0; k < inst.g_points.size(); k++) {
        if (std::find(indices.begin(), indices.end(), k) == indices.end()) {
            QtPoint& point = inst.g_points[k];
            std::vector<std::tuple<int, int>> label_positions = point.getPossibleLabelPositions();
            std::random_shuffle(label_positions.begin(), label_positions.end());
            for (std::tuple<int, int> label_pos : label_positions) {
                if (inst.add(point, label_pos)) {
                    break;  // go to next point after succesful insertion
                }
            }
        }
    }
    for (QtPoint& p : inst.g_points) {
        if (p.g_exists && rand() % 100 > 70) {
            std::vector<std::tuple<int, int>> label_positions = p.getPossibleLabelPositions();
            std::random_shuffle(label_positions.begin(), label_positions.end());
            for (std::tuple<int, int> label_pos : label_positions) {
                if (inst.add(p, label_pos)) {
                    break;  // go to next point after succesful insertion
                }
            }
        }
    }
}

//copy one random parents solution to child
//at a crossover point the solution of the other parent is tried to be copied as well
//its random if the first or the second half of the child is changed
std::shared_ptr<Instance> crossover(Instance& parent0, Instance& parent1, int iteration) {
    std::srand(std::time(0) + iteration * 27);
    int crossoverPoint = (rand() % parent0.g_points.size()) + 1;

    //parent 0 or parent 1
    // TODO: in original child was set to primary parent
    int chosenParent = rand() % 2;
    Instance& primaryParent = chosenParent == 0 ? parent0 : parent1;
    Instance& secondaryParent = !(chosenParent == 1) ? parent0 : parent1;         // equal signs not really neede but easier to read
    std::shared_ptr<Instance> child(new Instance(primaryParent.g_points, true));  // adopt solution from parent

    //change first or second part of child first??
    // TODO: What does 'first' mean? should both parts be edited? cause they aren't in the original.
    int toChange = rand() % 2;
    int start = toChange == 0 ? 0 : crossoverPoint;
    int end = toChange == 0 ? crossoverPoint : child->g_points.size();

    for (int i = start; i < end; i++) {
        if (!secondaryParent.g_points[i].g_exists) {
            child->remove(child->g_points[i]);
        } else {
            int p_x = secondaryParent.g_points[i].g_solution_x;
            int p_y = secondaryParent.g_points[i].g_solution_y;
            // try to changed the i-th point of child if it differs from parent
            if (child->g_points[i].g_exists == false || child->g_points[i].g_solution_x != p_x || child->g_points[i].g_solution_y != p_y) {
                child->add(child->g_points[i], std::make_tuple(p_x, p_y));
                // small change to original: if new label cannot be set we just leave the old label like it was
                // instead of deleting it. TODO: is it bad?
            }
        }
    }
    return child;
}

//tries to set a point at the position if possible
//the positioning of the label is random
void setPointAtPos(int pos, Instance& inst, int iteration) {
    std::srand(std::time(0) + iteration * 18);

    QtPoint& point = inst.g_points[pos];
    std::vector<std::tuple<int, int>> label_positions = point.getPossibleLabelPositions();
    std::random_shuffle(label_positions.begin(), label_positions.end());
    for (std::tuple<int, int> label_pos : label_positions) {
        if (inst.add(point, label_pos)) {
            break;  // go to next point after succesful insertion
        }
    }
}

//return first population
//begin to set points at random position to the end
//then set from the beginning to that point
std::vector<std::shared_ptr<Instance>> getInitialPopulation(Instance& inst, int sizeOfPop) {
    std::srand(std::time(0));
    int count = 0;
    std::vector<std::shared_ptr<Instance>> initialSolutions;
    for (int i = 0; i < sizeOfPop; i++) {
        initialSolutions.push_back(std::shared_ptr<Instance>(new Instance(inst.g_points)));
    }
    assert(&initialSolutions.front() != &initialSolutions.back());

    int n = inst.g_points.size();
    for (int i = 0; i < sizeOfPop; i++) {
        int crossoverpoint = (rand() % n) + 1;
        for (int j = crossoverpoint; j < n; j++) {
            setPointAtPos(j, *initialSolutions[i], count);
            count++;
        }

        for (int j = 0; j < crossoverpoint; j++) {
            setPointAtPos(j, *initialSolutions[i], count);
            count++;
        }
    }
    assert(&initialSolutions.front() != &initialSolutions.back());

    return initialSolutions;
}

//return the index of best solution of the current population
std::vector<Point> getBestSolution(std::vector<std::shared_ptr<Instance>>& population) {
    int best_index = 0;

    for (int i = 1; i < population.size(); i++) {
        if (population[i]->getCount() > population[best_index]->getCount()) {
            best_index = i;
        }
    }
    std::vector<Point> solution;
    for (QtPoint p : population[best_index]->g_points) {
        solution.push_back(p.to_point());
    }

    return solution;
}

// only helper function
//void printPopulation(std::vector<std::vector<Point>>& population) {
//    for (int i = 0; i < population.size(); i++) {
//        for (int j = 0; j < population[i].size(); j++) {
//            std::cout << population[i][j].g_exists << ", ";
//        }
//    }
//    std::cout << "\n";
//}

std::vector<Point> QtGeneticAlgorithm::solve(std::vector<Point>& points) {
    Instance inst(points);
    std::srand(std::time(0));
    int sizeOfInitPop = 20;
    std::vector<std::shared_ptr<Instance>> population = getInitialPopulation(inst, sizeOfInitPop);
    int count = 0;

    while (count < g_max_iterations) {
        //selection
        std::sort(population.begin(), population.end(), [](std::shared_ptr<Instance>& a, std::shared_ptr<Instance>& b) { return a->getCount() > b->getCount(); });

        population.erase(population.begin() + (population.size() + 1) / 2, population.end());

        //population now smaller
        int currentSizeOfPop = population.size();
        
        auto rng = std::default_random_engine{};
        std::shuffle(std::begin(population), std::end(population), rng);
        //crossover
        //TODO: only loving your neighbors intended?
        for (int i = 1; i < currentSizeOfPop; i++) {
            std::shared_ptr<Instance> child = crossover(*population[i - 1], *population[i], count);
            //mutation
            if (rand() % 100 > 90) {
                mutation(*child, count);
            }

            //add child to current population
            population.push_back(std::move(child));
        }

        count++;
    }
    return getBestSolution(population);
}