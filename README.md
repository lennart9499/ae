# Algorithm Engineering


# Building

## Download already built binary

It's also possible to directly download an artifact file with the newest master release under the following Link:

[https://gitlab.com/lennart9499/ae/-/pipelines?page=1&scope=all&ref=master](https://gitlab.com/lennart9499/ae/-/pipelines?page=1&scope=all&ref=master)


## Compiling

1. Call `cmake .` in the main directory
2. Call `make`
3. Executable (`./ae`) is built and linked  

# Using

## Minumum execution command:
This uses the standard solver (betterbasic)
./ae -in INPUTFILE -out OUTPUTFILE

## Other command examples

These are our two main heuristics from Task2
```
./ae -in INPUTFILE -out OUTPUTFILE -solver qtSimAnn -r 3
./ae -in INPUTFILE -out OUTPUTFILE -solver qtGenAlg -r 3
```
And just for completeness the extended standard call
```
./ae -in INPUTFILE -out OUTPUTFILE -solver betterbasic
```

#### All arguments
Each with value range

TODO



