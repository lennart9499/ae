#ifndef AE_HeurSol1_H
#define AE_HeurSol1_H

#include <vector>
#include <src/util/Point.h>
#include <map>
#include "BaseSolver.h"
#include <iostream>
#include <algorithm>
#include <utility>
#include <bits/stdc++.h>
#include <tuple>
#include <cstdlib>


class HeuristicSolver1 : public BaseSolver {
public:
	std::vector<Point> solve(std::vector<Point> &points) override;
};

#endif
