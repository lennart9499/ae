//
// Created by lennart on 28.10.2020.
//

#include <time.h>
#include "RandomSolver.h"

std::vector<Point> RandomSolver::solve(std::vector<Point> &points) {

    //TODO will mal das random haben?
    //srand (time(NULL));
    srand(303287710);

    int points_with_label = 0;

    //alle exists auf false setzen
    for (Point &point : points) {
        point.g_exists = false;
        point.g_solution_x = 0;
        point.g_solution_y = 0;
    }

    //alle exists auf false setzen
    bool problem;
    for (std::vector<Point>::size_type i = 0; i < points.size(); i++) {
        Point &point = points[i];

        std::vector<std::tuple<int, int>> positions = point.getPossibleLabelPositions();
        //change random
        for (int i = 0; i < 4; i++){
            int pos1 = rand() % 4;
            int pos2 = rand() % 4;
            auto temp = positions[pos1];
            positions[pos1] = positions[pos2];
            positions[pos2] = temp;
        }

        //check all possible label positions:
        for(int c = 0; c < 4; c++) {
            point.g_exists = true;
            point.g_solution_x = std::get<0>(positions[c]);
            point.g_solution_y = std::get<1>(positions[c]);

            problem = false;
            for (std::vector<Point>::size_type j = 0; j < points.size(); j++) {
                Point &other = points[j];
                if (other.g_exists && i != j && Point::checkOverlap(point, other)) {
                    problem = true;
                    break;
                }
            }
            if (problem) {
                point.g_exists = false;
                point.g_solution_x = 0;
                point.g_solution_y = 0;
            }else{
                break;
            }
        }
    }

    return points;
}
