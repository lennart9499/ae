//
// Created by lennart on 28.10.2020.
//

#ifndef AE_BASICSOLVER_H
#define AE_BASICSOLVER_H


#include <vector>
#include <src/util/Point.h>
#include "BaseSolver.h"

class BasicSolver : public BaseSolver {
public:
    std::vector<Point> solve(std::vector<Point> &points) override;
};


#endif //AE_BASICSOLVER_H
