#ifndef AE_INSTANCE_H
#define AE_INSTANCE_H

#include <fstream>
#include <iostream>
#include <regex>
#include <string>

#include "Point.h"
#include "QtPoint.h"
#include "Quadtree.h"
/* 
This class represents a problem instance. It stores the
enabled labels in a quadtree to achieve faster collision detection.
Initially the points are ordered like in the input file. No method
changes this order, so it can be reliably changed from the outside. 
DON'T change the point's values directly and call functions with
the real points; not copies. Always use the offered methods
to keep the tree up-to-date. This guarantess that at all times no 
collision occurs between the labels in tree and the solution is viable!
INVARIANTS:
    - for all points p: p.g_exists == true iff p is in the tree
    - the solution represented by this instance is viable
*/
class Instance {
   public:
   
    std::vector<QtPoint> g_points;
    Quadtree g_qt;
    int labeled_points = 0;
   
    /* If adopt_sol == false the solution's values will be ignored init_points will be called.
       If adopt_sol == true g_exists, g_solution_x/y of the points won't be changed and
       we try to insert the points with these values. If there's a problem/collision
       something will be printed to std::out.
    */
    Instance(std::string& file, bool adopt_sol = false);
    Instance(std::vector<QtPoint>& points, bool adopt_sol = false);
    Instance(std::vector<Point>& points, bool adopt_sol = false);

    /* Tries to add the given point p to the solution with its label at label_pos.
       If a label was already set for this point it will be removed.
       Returns true if the insertion was succesful and keeps changes.
       Returns false if a collision occured and reverts changes. 
       The point's members are set accordingly.*/
    bool add(QtPoint& p, const std::tuple<int, int>& label_pos);

    /* Removes the point's label from the solution if possible
       and sets the point's members accordingly.*/
    void remove(QtPoint& p);

    /* Similiar to add but only as check. Won't change anything.
       Returns true if the insertion would be viable.*/
    bool check(QtPoint& p, std::tuple<int, int> label_pos);

    /* Writes the current solution to the given file. 
       NOT TESTED, JUST COPIED!*/
    void serialize(std::string& file);

    /* Returns number of set labels. Equal to number of
       points p in g_points with p.g_exists == true. */
    int getCount();

    /* Returns the percentage of labels set. */
    double getCoverage();

    /* Resets the labels and the tree to default values so the instance can
       can be solved again. NOTE: The point order won't be reset! 
       NOT YET IMPLEMENTED SINCE NOT NEEDED!*/
    void reset();

    /* Sets g_exists = false, p.g_solution_x = 0, p.g_solution_y = 0 
       for all points in g_points.*/
    void init_points();

    /* Inits tree and adjusts points according to constructor's promises.*/
    void init_tree(bool adopt_sol = false);
    
    
    /* The brute force overlap check from points. Only
       for debugging.*/
    Point::Feasibility brute_check_all_overlaps() {
        for (QtPoint& point1 : g_points) {
            for (QtPoint& point2 : g_points) {
                if (point1 != point2) {
                    if (point1.g_exists && point2.g_exists) {
                        if (point1.checkOverlap(point2)) {
                            std::cerr << "ERROR: Between label \"" << point1.g_name << "\" and \"" << point2.g_name
                                      << "\"" << std::endl;
                            return Point::Feasibility::OVERLAP_ERROR;
                        }
                    }
                }
            }
        }
        return Point::Feasibility::OK;
    }

    /* Checks for consistency. Only for debugging. 
       Can be used with assert and should never 
       return false (hopefully :D).*/
    bool check_consistency() {
        int cnt = 0;
        for (QtPoint& p : g_points) {
            if (p.g_exists) cnt++;
        }
        if (cnt != getCount()) {
            printf("cnt != getCount() :(");
            fflush(stdout);
            return false;
        }
        std::vector<QtPoint*> tp = g_qt.get_points();
        for (QtPoint& p : g_points) {
            if (p.g_exists && std::find(tp.begin(), tp.end(), &p) == tp.end()) {
                printf("Existing point that is not in tree.:(");
                fflush(stdout);
                return false;
            } else if (!p.g_exists && std::find(tp.begin(), tp.end(), &p) != tp.end()) {
                printf("Non-Existing point that is in tree.:(");
                fflush(stdout);
                return false;
            }
        }

        return true;
    }
};

#endif  //AE_INSTANCE_H
