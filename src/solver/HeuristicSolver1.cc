
#include "HeuristicSolver1.h"


struct PointComp{
		
	bool operator() (const Point& p1, const Point& p2) const{
		
		int labelVolumeP1 = p1.g_width * p1.g_height;
		int labelVolumeP2 = p2.g_width * p2.g_height;
		
				
		return (labelVolumeP1 < labelVolumeP2);
	}

};


bool testLabel(std::vector<Point>& points, Point& point, std::string position){
	
	point.g_exists = true;
	
	if(position.compare("topRight")){
		point.g_solution_x = std::get<0>(point.getPossibleLabelPositions()[2]);
            	point.g_solution_y = std::get<1>(point.getPossibleLabelPositions()[2]);		
	}

	else if(position.compare("topLeft")){
		point.g_solution_x = std::get<0>(point.getPossibleLabelPositions()[3]);
            	point.g_solution_y = std::get<1>(point.getPossibleLabelPositions()[3]);
	}

	else if(position.compare("bottomRight")){
		point.g_solution_x = std::get<0>(point.getPossibleLabelPositions()[0]);
            	point.g_solution_y = std::get<1>(point.getPossibleLabelPositions()[0]);
	}

	else if (position.compare("bottomLeft")){
		point.g_solution_x = std::get<0>(point.getPossibleLabelPositions()[1]);
            	point.g_solution_y = std::get<1>(point.getPossibleLabelPositions()[1]);
	}

	bool labelSuccess = true;
	for(int i=0; i<points.size(); i++){
		if(points[i].g_exists && points[i] != point && Point::checkOverlap(point, points[i])){
			labelSuccess = false;
			break;
		}
	}
	if(!labelSuccess){
		point.g_exists = false;
		point.g_solution_x = 0;
		point.g_solution_y = 0;
	}
	return labelSuccess;
	
}


void setLabels(std::vector<Point>& points){
	
	//get size of field
	int x_max = INT_MIN;
	int y_max = INT_MIN;

	int x_min = INT_MAX;
	int y_min = INT_MAX;	
	for(int i=0; i<points.size(); i++){
		if(points[i].g_x > x_max) x_max = points[i].g_x;
		if(points[i].g_x < x_min) x_min = points[i].g_x;
		if(points[i].g_y > y_max) y_max = points[i].g_y;
		if(points[i].g_y < y_min) y_min = points[i].g_y;		

	}

	int x_size = abs(x_max) + abs(x_min);
	int y_size = abs(y_max) + abs(y_min);
	
	//set search radii
	int searchRadiusX = x_size / 10;
	int searchRadiusY = y_size / 10;



	for(int i=0; i<points.size(); i++){
		//get numberOfPoints in search radius for every possible label pos
		int points_topRight = 0;
		int points_topLeft = 0;
		int points_bottomRight = 0;
		int points_bottomLeft = 0;
		
		for(int j=0; j<points.size(); j++){
			
			if(i!=j){
				//topRight:			
				if((points[i].g_x + searchRadiusX >= points[j].g_x) && (points[i].g_y + searchRadiusY >= points[j].g_y) && (points[i].g_x <= points[j].g_x) && (points[i].g_y <= points[j].g_y)){
					points_topRight++;
				}
				//topLeft:
				if((points[i].g_x - searchRadiusX <= points[j].g_x) && (points[i].g_y + searchRadiusY >= points[j].g_y) && (points[i].g_x >= points[j].g_x) && (points[i].g_y <= points[j].g_y)){
					points_topLeft++;
				
				}
				//bottonRight:
				if((points[i].g_x + searchRadiusX >= points[j].g_x) && (points[i].g_y - searchRadiusY <= points[j].g_y) && (points[i].g_x <= points[j].g_x) && (points[i].g_y >= points[j].g_y)){
					points_bottomRight++;			

				}
				//bottonLeft:
				if((points[i].g_x - searchRadiusX <= points[j].g_x) && (points[i].g_y - searchRadiusY <= points[j].g_y) && (points[i].g_x >= points[j].g_x) && (points[i].g_y >= points[j].g_y)){
					points_bottomLeft++;
				}
			}

		}

		std::vector<std::tuple<int, std::string>> sortedPositions (4);
		sortedPositions[0] = std::make_tuple(points_topRight, "topRight");
		sortedPositions[1] = std::make_tuple(points_topLeft, "topLeft");
		sortedPositions[2] = std::make_tuple(points_bottomRight, "bottomRight");
		sortedPositions[3] = std::make_tuple(points_bottomLeft, "bottomLeft");
		
		
		sort(sortedPositions.begin(), sortedPositions.end());
		
		for(int j=0; j<4; j++){		
			if(testLabel(points, points[i], std::get<1>(sortedPositions[j]))){
				break;
			}
		}

		
	}
}





std::vector<Point> HeuristicSolver1::solve(std::vector<Point>& points){
	for (Point &point : points) {
        	point.g_exists = false;
        	point.g_solution_x = 0;
        	point.g_solution_y = 0;
    	}
	
	//sort points
	std::sort(points.begin(), points.end(), PointComp());

	setLabels(points);


	return points;
	

}


