import subprocess

if __name__ == "__main__":
    print("Test")

    # TODO create different configs also linux
    # ALSO repeat quote:

    repeat = 1

    config_file_path = "benchmark_configs/default.config"

    import sys

    files = []
    if len(sys.argv) > 1:
        config_file_path = sys.argv[1]

    import os
    if(os.path.isfile(config_file_path)):
        files.append(config_file_path)
    else:
        for (dirpath, dirnames, filenames) in os.walk(config_file_path):
            for f in filenames:
                files.append(config_file_path + f)
            break


    print("Found configs: ", len(files))

    for config_file in files:
        print("CONFIG:", config_file)

        list_benchmark_files = None
        list_program_calls = None

        import json
        with open(config_file) as json_file:
            data = json.load(json_file)
            list_benchmark_files = data['benchmark_files']
            list_program_calls = data['program_calls']

        data_time = []
        data_count = []

        for i, x in enumerate(list_benchmark_files):
            temp_time = []
            temp_count = []
            for j, y in enumerate(list_program_calls):
                avg_time_sum = 0
                avg_obj_sum = 0
                for o in range(repeat):
                    print(y.replace("%inputfile%", x))
                    result = subprocess.run(y.replace("%inputfile%", x), stdout=subprocess.PIPE, shell=True)
                    avg_time_sum += float(result.stdout.decode('utf-8').split("\t")[1])
                    avg_obj_sum += int(result.stdout.decode('utf-8').split("\t")[0])
                temp_time.append(float(avg_time_sum / repeat))
                temp_count.append(int(avg_obj_sum / repeat))  # Maybe keep double
            data_time.append(temp_time)
            data_count.append(temp_count)
        print(data_time)
        print(data_count)
