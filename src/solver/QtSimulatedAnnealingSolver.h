#ifndef AE_QtSimAnn_H
#define AE_QtSimAnn_H

#include <bits/stdc++.h>
#include <math.h>
#include <src/util/Instance.h>
#include <src/util/Point.h>

#include <algorithm>
#include <chrono>
#include <cstdlib>
#include <ctime>
#include <iostream>
#include <map>
#include <tuple>
#include <utility>
#include <vector>

#include "BaseSolver.h"

/*
    A version of Tim's 'SimulatedAnnealingSolver' which uses 'Instance' and therefore
    quadtrees for collision detection. Should be a little slower on small instances,
    but much faster/viable on large instances. The only other real difference is that
    the possible label positions are randomized before insertion attempts. This is 
    currently a bug in the original since not all solutions will be considered.
    
    ./ae -in instances/random/random_100000_d0_5 -out out.txt -solver qtSimAnn
    44736   35.92 (Don't know how man iterations, but not enough :D.)
    
    ./ae -in instances/random/random_100000_d0_5 -out out.txt -solver SimAnn
    Initial solution: 43351
    41693   240.90 (Should be the time for the inital solution + one iteration)
    
    TODOs: 
    - the hardcoded number of iterations and maximal computing time could by computed dynamically
        (dependent on instance size)
    - are the other parameters a good choice or random (@Tim)? (Haven't tested and just copied from the original)
*/
class QtSimulatedAnnealingSolver : public BaseSolver {
   public:
    int g_max_iterations;
    QtSimulatedAnnealingSolver(int max_iterations){
        g_max_iterations = max_iterations >= 0 ? max_iterations : 3000;
    }
    std::vector<Point> solve(std::vector<Point> &points) override;
};

#endif
