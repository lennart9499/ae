#include "QtSimulatedAnnealingSolver.h"

std::shared_ptr<Instance> getNeighbor(std::vector<QtPoint> const &ipoints, int iterations) {
    std::vector<QtPoint> tmp_points;
    for (QtPoint p : ipoints) {
        tmp_points.push_back(QtPoint(p));
    }
    std::shared_ptr<Instance> neighbor(new Instance(tmp_points, true));
    std::vector<QtPoint> &points = neighbor->g_points;

    int range = points.size() * 0.2;

    std::srand(std::time(0) + iterations * 123);

    int undoPoints = (rand() % range) + 1;

    std::vector<int> indices(undoPoints);

    //deleteLabel for random number of points
    for (int i = 0; i < undoPoints; i++) {
        int pointToUndo = (rand() % points.size()) + 1;

        //(check that the point is set before TODO???)

        indices[i] = pointToUndo;
        neighbor->remove(points[pointToUndo]);
    }

    //look if other points can be set
    for (std::vector<QtPoint>::size_type i = 0; i < points.size(); i++) {
        if (!(std::find(indices.begin(), indices.end(), i) != indices.end())) {
            QtPoint &point = points[i];

            //check all possible label positions:
            std::vector<std::tuple<int, int>> label_positions = point.getPossibleLabelPositions();
            std::random_shuffle(label_positions.begin(), label_positions.end());
            for (std::tuple<int, int> label_pos : label_positions) {
                if (neighbor->add(point, label_pos)) {
                    break;  // go to next point after succesful insertion
                }
            }
        }
    }

    return neighbor;
}

void getFirstSol(Instance &inst) {
    std::vector<QtPoint> &points = inst.g_points;
    for (std::vector<QtPoint>::size_type i = 0; i < points.size(); i++) {
        QtPoint &point = points[i];
        //check all possible label positions:
        std::vector<std::tuple<int, int>> label_positions = point.getPossibleLabelPositions();
        std::random_shuffle(label_positions.begin(), label_positions.end());
        for (std::tuple<int, int> label_pos : label_positions) {
            if (inst.add(point, label_pos)) {
                break;  // go to next point after succesful insertion
            }
        }
    }
    //printf("Initial solution: %d\n", inst.getCount());
}

std::vector<Point> QtSimulatedAnnealingSolver::solve(std::vector<Point> &points) {
    std::shared_ptr<Instance> inst(new Instance(points));
    std::shared_ptr<Instance> optimal_solution = inst;
    getFirstSol(*inst);

    double temp = 30.0;
    int iterations = 0;

    auto start = std::chrono::high_resolution_clock::now();
    auto currentTime = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> elapsed = currentTime - start;

    std::shared_ptr<Instance> neighbor;

    while (iterations < g_max_iterations && elapsed.count() < 60*5) { //30
        neighbor = std::move(getNeighbor(inst->g_points, iterations));
        if(neighbor->getCount()>optimal_solution->getCount()){
            optimal_solution = neighbor;
        }
        double diff = (neighbor->getCoverage() - inst->getCoverage()) * 100.0;
        if (diff >= 0) {
            inst = std::move(neighbor);
        }
        //diff < 0
        else {
            double r = rand();
            if ((r / RAND_MAX) <= exp((diff) / temp)) {
                inst = std::move(neighbor);
            }
        }

        temp = temp * 0.98;
        iterations++;
        currentTime = std::chrono::high_resolution_clock::now();
        elapsed = currentTime - start;
    }

    std::vector<Point> solution;
    for (QtPoint p : optimal_solution->g_points) {
        solution.push_back(p.to_point());
    }

    return solution;
}
