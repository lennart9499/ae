#ifndef AE_SimAnn_H
#define AE_SimAnn_H

#include <bits/stdc++.h>
#include <math.h>
#include <src/util/Point.h>

#include <algorithm>
#include <chrono>
#include <cstdlib>
#include <ctime>
#include <iostream>
#include <map>
#include <tuple>
#include <utility>
#include <vector>

#include "BaseSolver.h"

class SimulatedAnnealingSolver : public BaseSolver {
   public:
    std::vector<Point> solve(std::vector<Point> &points) override;
};

#endif
