#ifndef AE_QtGenAlg_H
#define AE_QtGenAlg_H

#include <bits/stdc++.h>
#include <math.h>
#include <src/util/Instance.h>
#include <src/util/Point.h>

#include <algorithm>
#include <chrono>
#include <cstdlib>
#include <ctime>
#include <iostream>
#include <map>
#include <random>
#include <tuple>
#include <utility>
#include <vector>

#include "BaseSolver.h"

class QtGeneticAlgorithm : public BaseSolver {
    int g_max_iterations;

   public:
    QtGeneticAlgorithm(int max_iterations) {
        g_max_iterations = max_iterations >= 0 ? max_iterations : 1000;
    }
    std::vector<Point> solve(std::vector<Point> &points) override;
};

#endif
