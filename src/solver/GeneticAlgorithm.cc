#include "GeneticAlgorithm.h"

//change some set of points and change some label positions
void mutation(std::vector<Point>& points, int iteration) {
    std::srand(std::time(0) + iteration * 27);

    int range = points.size() * 0.02;

    int undoPoints = (rand() % range) + 1;

    std::vector<int> indices(undoPoints);

    //deleteLabel for random number of points
    for (int i = 0; i < undoPoints; i++) {
        int pointToUndo = (rand() % points.size()) + 1;

        indices[i] = pointToUndo;
        points[pointToUndo].g_exists = false;
        points[pointToUndo].g_solution_x = 0;
        points[pointToUndo].g_solution_y = 0;
    }
    bool problem;
    //look if other points can be set
    for (int k = 0; k < points.size(); k++) {
        if (!(std::find(indices.begin(), indices.end(), k) != indices.end())) {
            Point& point = points[k];
            for (int c = 0; c < 4; c++) {
                point.g_exists = true;
                point.g_solution_x = std::get<0>(point.getPossibleLabelPositions()[c]);
                point.g_solution_y = std::get<1>(point.getPossibleLabelPositions()[c]);

                problem = false;
                for (std::vector<Point>::size_type j = 0; j < points.size(); j++) {
                    Point& other = points[j];
                    if (other.g_exists && k != j && Point::checkOverlap(point, other)) {
                        problem = true;
                        break;
                    }
                }
                if (problem) {
                    point.g_exists = false;
                    point.g_solution_x = 0;
                    point.g_solution_y = 0;
                } else {
                    break;
                }
            }
        }
    }

    std::vector<int> randomPositions{0, 1, 2, 3};
    std::random_shuffle(randomPositions.begin(), randomPositions.end());
    //change labels of points if possible
    for (int i = 0; i < points.size(); i++) {
        if (rand() % 100 > 70 && points[i].g_exists) {
            Point& point = points[i];
            for (int c = 0; c < 4; c++) {
                point.g_exists = true;
                point.g_solution_x = std::get<0>(point.getPossibleLabelPositions()[randomPositions[c]]);
                point.g_solution_y = std::get<1>(point.getPossibleLabelPositions()[randomPositions[c]]);

                problem = false;
                for (std::vector<Point>::size_type j = 0; j < points.size(); j++) {
                    Point& other = points[j];
                    if (other.g_exists && i != j && Point::checkOverlap(point, other)) {
                        problem = true;
                        break;
                    }
                }
                if (problem) {
                    point.g_exists = false;
                    point.g_solution_x = 0;
                    point.g_solution_y = 0;
                } else {
                    break;
                }
            }
        }
    }
}

//copy one random parents solution to child
//at a crossover point the solution of the other parent is tried to be copied as well
//its random if the first or the second half of the child is changed
std::vector<Point> crossover(std::vector<Point>& parent0, std::vector<Point>& parent1, int iteration) {
    std::srand(std::time(0) + iteration * 27);
    int crossoverPoint = (rand() % parent0.size()) + 1;
    std::vector<Point> child;
    //parent 0 or parent 1
    int choosenParent = rand() % 2;
    if (choosenParent == 0) {
        child = parent0;
    } else if (choosenParent == 1) {
        child = parent1;
    }

    //change first or second part of child first??
    int toChange = rand() % 2;

    //change beginning
    if (toChange == 0) {
        if (choosenParent == 0) {
            for (int i = 0; i < crossoverPoint; i++) {
                if (parent1[i].g_exists == false) {
                    child[i] = parent1[i];
                } else {
                    child[i] = parent1[i];
                    //check if possible

                    bool problem = false;
                    for (std::vector<Point>::size_type j = 0; j < child.size(); j++) {
                        Point& other = child[j];
                        if (other.g_exists && i != j && Point::checkOverlap(child[i], other)) {
                            problem = true;
                            break;
                        }
                    }
                    if (problem) {
                        child[i].g_exists = false;
                        child[i].g_solution_x = 0;
                        child[i].g_solution_y = 0;
                    }
                }
            }
        } else if (choosenParent == 1) {
            for (int i = 0; i < crossoverPoint; i++) {
                if (parent0[i].g_exists == false) {
                    child[i] = parent0[i];
                } else {
                    child[i] = parent0[i];
                    //check if possible
                    bool problem = false;
                    for (std::vector<Point>::size_type j = 0; j < child.size(); j++) {
                        Point& other = child[j];
                        if (other.g_exists && i != j && Point::checkOverlap(child[i], other)) {
                            problem = true;
                            break;
                        }
                    }
                    if (problem) {
                        child[i].g_exists = false;
                        child[i].g_solution_x = 0;
                        child[i].g_solution_y = 0;
                    }
                }
            }
        }

        //change end
    } else if (toChange == 1) {
        if (choosenParent == 0) {
            for (int i = crossoverPoint; i < parent0.size(); i++) {
                if (parent1[i].g_exists == false) {
                    child[i] = parent1[i];
                } else {
                    child[i] = parent1[i];
                    //check if possible

                    bool problem = false;
                    for (std::vector<Point>::size_type j = 0; j < child.size(); j++) {
                        Point& other = child[j];
                        if (other.g_exists && i != j && Point::checkOverlap(child[i], other)) {
                            problem = true;
                            break;
                        }
                    }
                    if (problem) {
                        child[i].g_exists = false;
                        child[i].g_solution_x = 0;
                        child[i].g_solution_y = 0;
                    }
                }
            }
        } else if (choosenParent == 1) {
            for (int i = crossoverPoint; i < parent0.size(); i++) {
                if (parent0[i].g_exists == false) {
                    child[i] = parent0[i];
                } else {
                    child[i] = parent0[i];
                    //check if possible
                    bool problem = false;
                    for (std::vector<Point>::size_type j = 0; j < child.size(); j++) {
                        Point& other = child[j];
                        if (other.g_exists && i != j && Point::checkOverlap(child[i], other)) {
                            problem = true;
                            break;
                        }
                    }
                    if (problem) {
                        child[i].g_exists = false;
                        child[i].g_solution_x = 0;
                        child[i].g_solution_y = 0;
                    }
                }
            }
        }
    }

    return child;
}

//get fitness by looking at the percentage of set points
int fitness(std::vector<Point>& points) {
    double solvedPointCount = 0.0;

    for (Point& point : points) {
        if (point.g_exists == true) {
            solvedPointCount++;
        }
    }

    int percentage = (solvedPointCount / points.size()) * 100;

    return percentage;
}

//tries to set a point at the position if possible
//the positioning of the label is random
void setPointAtPos(int pos, std::vector<Point>& points, int iteration) {
    std::srand(std::time(0) + iteration * 18);

    bool problem;

    std::vector<int> randomPositions{0, 1, 2, 3};
    std::random_shuffle(randomPositions.begin(), randomPositions.end());

    Point& point = points[pos];
    for (int c = 0; c < 4; c++) {
        point.g_exists = true;
        point.g_solution_x = std::get<0>(point.getPossibleLabelPositions()[randomPositions[c]]);
        point.g_solution_y = std::get<1>(point.getPossibleLabelPositions()[randomPositions[c]]);

        problem = false;
        for (std::vector<Point>::size_type j = 0; j < points.size(); j++) {
            Point& other = points[j];
            if (other.g_exists && pos != j && Point::checkOverlap(point, other)) {
                problem = true;
                break;
            }
        }
        if (problem) {
            point.g_exists = false;
            point.g_solution_x = 0;
            point.g_solution_y = 0;
        } else {
            break;
        }
    }
}

//return first population
//begin to set points at random position to the end
//then set from the beginning to that point
std::vector<std::vector<Point>> getInitialPopulation(std::vector<Point>& points, int sizeOfPop) {
    std::srand(std::time(0));

    int count = 0;
    std::vector<std::vector<Point>> initialSolutions(sizeOfPop, points);
    for (int i = 0; i < sizeOfPop; i++) {
        int crossoverpoint = (rand() % points.size()) + 1;
        for (int j = crossoverpoint; j < points.size(); j++) {
            setPointAtPos(j, initialSolutions[i], count);
            count++;
        }

        for (int j = 0; j < crossoverpoint; j++) {
            setPointAtPos(j, initialSolutions[i], count);
            count++;
        }
    }

    return initialSolutions;
}

//get the index of the worst solution, which is in the population
int getIndexOfWorst(std::vector<std::vector<Point>>& population) {
    int worstIndex = 0;

    for (int i = 0; i < population.size(); i++) {
        if (fitness(population[i]) < fitness(population[worstIndex])) {
            worstIndex = i;
        }
    }
    return worstIndex;
}

//delete the worst half of the population
void selection(std::vector<std::vector<Point>>& population) {
    int popSize = population.size();
    for (int i = 0; i < popSize / 2; i++) {
        population.erase(population.begin() + getIndexOfWorst(population));
    }
}

//return the best solution of the current population
std::vector<Point> getBestSolution(std::vector<std::vector<Point>>& population) {
    std::vector<Point> bestSol = population[0];

    for (int i = 0; i < population.size(); i++) {
        if (fitness(population[i]) > fitness(bestSol)) {
            bestSol = population[i];
        }
    }

    return bestSol;
}

void printPopulation(std::vector<std::vector<Point>>& population) {
    for (int i = 0; i < population.size(); i++) {
        for (int j = 0; j < population[i].size(); j++) {
            std::cout << population[i][j].g_exists << ", ";
        }
    }
    std::cout << "\n";
}

std::vector<Point> GeneticAlgorithm::solve(std::vector<Point>& points) {
    std::srand(std::time(0));
    int sizeOfInitPop = 20;
    std::vector<std::vector<Point>> population = getInitialPopulation(points, sizeOfInitPop);
    int count = 0;

    while (count < 1000) {
        //selection
        selection(population);
        //population now smaller

        std::vector<Point> child;
        int currentSizeOfPop = population.size();

        std::random_shuffle(population.begin(), population.end());
        //crossover
        for (int i = 1; i < currentSizeOfPop; i++) {
            std::vector<Point> parent0 = population[i - 1];
            std::vector<Point> parent1 = population[i];
            child = crossover(parent0, parent1, count);
            //mutation
            if (rand() % 100 > 90) {
                mutation(child, count);
            }
            //add child to current population
            population.push_back(child);
        }

        count++;
    }

    return getBestSolution(population);
}
