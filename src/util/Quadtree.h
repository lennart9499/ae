#ifndef AE_QUADTREE_H
#define AE_QUADTREE_H

#include <algorithm>
#include <cassert>
#include <memory>
#include <vector>

#include "QtPoint.h"

/*
    A naive Quadtree implementation that hopefully achieves fast collision detection.
    Each Node is either a leaf or has four children. Only leafs store
    labels and each leaf stores every label(as it's point) it overlaps 
    with up to 'CAPACITY' labels. Doesn't allow collisions between
    stored labels. (Could be changed later if necessary.)
    Currently the tree doesn't shrink after removals but this is probably ok.
*/
class Quadtree {
    // 17, 18 ?
    const int CAPACITY = 18;

   public:
    // ordinal directions used to index children
    enum OrdDir {
        SW,
        SE,
        NE,
        NW
    };

    std::unique_ptr<Quadtree> g_children[4] = {nullptr};
    
    // contains pointers to points of stored labels
    std::vector<QtPoint*> g_points;
    
    // coordinates of bounds and center of this tree
    int g_minx, g_maxx, g_miny, g_maxy, g_centerx, g_centery;

    Quadtree(int minx = 0, int maxx = 0, int miny = 0, int maxy = 0) {
        init(minx, maxx, miny, maxy);
    }
    
    // init bounds and center
    void init(int minx, int maxx, int miny, int maxy) {
        g_minx = minx;
        g_maxx = maxx;
        g_miny = miny;
        g_maxy = maxy;
        g_centerx = (minx + maxx) / 2;
        g_centery = (miny + maxy) / 2;
    }

    // consistent with add in Instance
    bool insert(QtPoint& p, const std::tuple<int, int>& label_pos);

    /* checks if the the point p can be inserted and returns true if so.
        This function is used internally by insert and often it is good choice
        to just use insert instead of check. Will store possible insert locations
        (leafs) in insert_candidates.*/
    bool check(QtPoint& p, const std::tuple<int, int>& label_pos, std::vector<Quadtree*> &insert_candidates);

    // removes the given point's label from the tree and sets g_exists to false.
    void remove(QtPoint& p);
    
    // Returns whether the tree is empty. Currently not used.
    bool empty();

    // Returns whether the tree is a leaf.
    bool is_leaf();
    
    /* Old function to print the tree for debugging purposes.
       No guarantees!    
    */
    void print_tree(bool show_borders);


    
    /* Returns pointers to all points currently stored in the tree.
       Shouldn't be needed unless for debugging purposes. 
    */
    std::vector<QtPoint*> get_points();

    // add a range query if necessary
    
    // add shrinking behaviour if necessary
    
   private:
    // Recursive function used by public check method
    bool check(QtPoint& p, std::vector<Quadtree*> &insert_candidates);

    // Check if two AABBs with corners (x0,y0), (x1,y1) and (x2,y2),(x3,y3) overlap or touch.
    // The first of the two vectors is always the upper left corner.
    bool aabbOverlap(int x0, int y0, int x1, int y1, int x2, int y2, int x3, int y3);

    // actually inserts the point. Used by insert.
    void append(QtPoint& p);

    // Creates four empty trees as children.
    void subdivide();

    // Used by print_tree
    void rec_print(int level, int child_id, bool show_borders);
};

#endif  //AE_QUADTREE_H