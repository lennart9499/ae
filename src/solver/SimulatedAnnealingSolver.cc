#include "SimulatedAnnealingSolver.h"

std::vector<Point> getNeighbor(std::vector<Point> points, int iterations) {
    int range = points.size() * 0.2;

    std::srand(std::time(0) + iterations * 123);

    int undoPoints = (rand() % range) + 1;

    std::vector<int> indices(undoPoints);

    //deleteLabel for random number of points
    for (int i = 0; i < undoPoints; i++) {
        int pointToUndo = (rand() % points.size()) + 1;

        //(check that the point is set before TODO???)

        indices[i] = pointToUndo;
        points[pointToUndo].g_exists = false;
        points[pointToUndo].g_solution_x = 0;
        points[pointToUndo].g_solution_y = 0;
    }
    bool problem;
    //look if other points can be set
    for (int k = 0; k < points.size(); k++) {
        if (!(std::find(indices.begin(), indices.end(), k) != indices.end())) {
            Point &point = points[k];
            // TODO: will always check labels in the same order
            for (int c = 0; c < 4; c++) {
                point.g_exists = true;
                point.g_solution_x = std::get<0>(point.getPossibleLabelPositions()[c]);
                point.g_solution_y = std::get<1>(point.getPossibleLabelPositions()[c]);

                problem = false;
                for (std::vector<Point>::size_type j = 0; j < points.size(); j++) {
                    Point &other = points[j];
                    if (other.g_exists && k != j && Point::checkOverlap(point, other)) {
                        problem = true;
                        break;
                    }
                }
                if (problem) {
                    point.g_exists = false;
                    point.g_solution_x = 0;
                    point.g_solution_y = 0;
                } else {
                    break;
                }
            }
        }
    }
    return points;
}

int getPercentageOfSetPoints(std::vector<Point> &points) {
    double solvedPointCount = 0.0;

    for (Point &point : points) {
        if (point.g_exists == true) {
            solvedPointCount++;
        }
    }

    int percentage = (solvedPointCount / points.size()) * 100;

    return percentage;
}

void getFirstSol(std::vector<Point> &points) {
    bool problem;
    for (std::vector<Point>::size_type i = 0; i < points.size(); i++) {
        Point &point = points[i];

        //check all possible label positions:
        for (int c = 0; c < 4; c++) {
            point.g_exists = true;
            point.g_solution_x = std::get<0>(point.getPossibleLabelPositions()[c]);
            point.g_solution_y = std::get<1>(point.getPossibleLabelPositions()[c]);

            problem = false;
            for (std::vector<Point>::size_type j = 0; j < points.size(); j++) {
                Point &other = points[j];
                if (other.g_exists && i != j && Point::checkOverlap(point, other)) {
                    problem = true;
                    break;
                }
            }
            if (problem) {
                point.g_exists = false;
                point.g_solution_x = 0;
                point.g_solution_y = 0;
            } else {
                break;
            }
        }
    }
    int cnt = 0;
    for (Point &p : points) {
        if (p.g_exists) cnt++;
    }
    printf("Initial solution: %d\n", cnt);
}

void printSolution(std::vector<Point> &points) {
    for (int i = 0; i < points.size(); i++) {
        std::cout << points[i].g_exists << ", ";
    }
    std::cout << "\n";
}

std::vector<Point> SimulatedAnnealingSolver::solve(std::vector<Point> &points) {
    for (Point &point : points) {
        point.g_exists = false;
        point.g_solution_x = 0;
        point.g_solution_y = 0;
    }

    getFirstSol(points);

    double temp = 30.0;
    int iterations = 0;

    auto start = std::chrono::high_resolution_clock::now();
    auto currentTime = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> elapsed = currentTime - start;

    while (iterations < 3000 && elapsed.count() < 30) {
        std::vector<Point> neighbor = getNeighbor(points, iterations);

        double diff = getPercentageOfSetPoints(neighbor) - getPercentageOfSetPoints(points);

        if (diff >= 0) {
            points = neighbor;
        }
        //diff < 0
        else {
            double r = rand();
            if ((r / RAND_MAX) <= exp((diff) / temp)) {
                points = neighbor;
            }
        }

        temp = temp * 0.98;
        iterations++;
        currentTime = std::chrono::high_resolution_clock::now();
        elapsed = currentTime - start;
    }

    return points;
}
