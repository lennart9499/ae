#!/usr/bin/python
import matplotlib.pyplot as plt
import matplotlib.patches as mpatch
import sys

fig, ax = plt.subplots()

rectangles = {}
points = {}

range_x = [0, 0]
range_y = [0, 0]

if len(sys.argv) > 1:
    with open(sys.argv[1], "r") as f:
        for x in f.readlines()[1:]:
            data = x.split()
            #print((int(data[6]), int(data[7]) - int(data[2])), int(data[2]), int(data[3]))
            if (int(data[5]) > 0):
                # lsg gefunden
                rectangles[data[4]] = mpatch.Rectangle((int(data[6]), int(data[7]) - int(data[3])), int(data[2]),
                                                       int(data[3]), edgecolor='black', linewidth=1, zorder=5)
                range_x[0] = min(range_x[0], int(data[6]))
                range_x[1] = max(range_x[1], int(data[6]) + int(data[2]))
                range_y[0] = min(range_y[0], int(data[7]) - int(data[3]))
                range_y[1] = max(range_y[1], int(data[7]))

                # grüner punkt plot  (int(data[0]),int(data[1])
                points[data[4]] = (True, int(data[0]), int(data[1]))
            else:
                print("Not possible:", int(data[0]), int(data[1]))
                points[data[4]] = (False, int(data[0]), int(data[1]))
                # nicht möglich / roter punkt plot

    for i, p in enumerate(points):
        #print(points[p][0])
        c = "green" if points[p][0] else "red"
        circle = plt.Circle((points[p][1], points[p][2]), radius=0.35, color=c, zorder=20)
        ax.add_patch(circle)
        ax.annotate(str(i + 1), xy=(points[p][1], points[p][2]), zorder=30, fontsize=8, color='w', weight='bold',
                    ha="center", va="center")

    for r in rectangles:
        ax.add_artist(rectangles[r])
        rx, ry = rectangles[r].get_xy()
        cx = rx + rectangles[r].get_width() / 2.0
        cy = ry + rectangles[r].get_height() / 2.0

        ax.annotate(r, (cx, cy), color='w', weight='bold',
                    fontsize=10, ha='center', va='center', zorder=10)

    ax.set_xlim((range_x[0] - 1, range_x[1] + 1))
    ax.set_ylim((range_y[0] - 1, range_y[1] + 1))
    ax.set_aspect('equal')

    plt.xticks(range(range_x[0] - 1, range_x[1] + 1))
    plt.yticks(range(range_y[0] - 1, range_y[1] + 1))

    plt.axhline(linewidth=2, color='gray')  # adds thick red line @ y=0
    plt.axvline(linewidth=2, color='gray')  # adds thick red line @ x=0

    plt.grid(b=True, which='major', color='#666666', linestyle='-')

    plt.show()
