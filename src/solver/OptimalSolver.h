//
// Created by lennart on 28.10.2020.
//

#ifndef AE_OPTIMALSOLVER_H
#define AE_OPTIMALSOLVER_H


#include "BaseSolver.h"

class OptimalSolver : public BaseSolver {
public:
    std::vector<Point> solve(std::vector<Point> &points) override ;

private:
    int fac(int vector_pos, std::vector<Point> &points, int &points_with_label_t, int legal_points,
            std::vector<Point> &points_best);
};


#endif //AE_OPTIMALSOLVER_H
