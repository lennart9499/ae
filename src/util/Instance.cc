#include "Instance.h"

Instance::Instance(std::string& file, bool adopt_sol) {
    // check file and init files like in Point
    int lines;

    std::ifstream infile(file);
    if (!infile.is_open()) {
        std::cerr << "ERROR: Input File does not exist!" << std::endl;
        exit(1);
    }

    //check malformed file, necessary?

    std::string fileLine;
    std::regex e_used;
    std::regex e_count("(-?\\d+)\\s?");
    std::regex e_other(
        "\\w?(-?\\d+)\\s+(-?\\d+)\\s+(-?\\d+)\\s+(-?\\d+)\\s+\\S+\\s?(\\s+(-?\\d+)\\s+(-?\\d+)\\s+(-?\\d+)\\s?)?");

    int linenumber = 0;
    int entry_cnt = 0;
    while (std::getline(infile, fileLine)) {
        linenumber++;
        // continue if the line's empty
        if (!fileLine.compare("\n")) {
            continue;
        }
        if (linenumber == 1) {
            e_used = e_count;
        } else {
            e_used = e_other;
            entry_cnt++;
        }
        if (!std::regex_match(fileLine, e_used)) {
            std::cerr << "ERROR: File not correctly formatted. Line: " << linenumber << std::endl;
            exit(1);
        }
    }

    //read file
    infile = std::ifstream(file);
    infile >> lines;

    if (lines != entry_cnt) {
        std::cerr << "ERROR: First line (count) does not correspond to the actual count! " << entry_cnt << " vs"
                  << lines << std::endl;
        exit(1);
    }

    int x, y, l, h;
    std::string t;
    int b_, x_, y_;

    //read data
    unsigned long long id = 0;
    while (infile >> x >> y >> l >> h >> t >> b_ >> x_ >> y_) {
        g_points.emplace_back(id++, x, y, l, h, t, b_, x_, y_);
    }
    init_tree(adopt_sol);
}

Instance::Instance(std::vector<QtPoint>& points, bool adopt_sol) {
    g_points = points;
    init_tree(adopt_sol);
}

Instance::Instance(std::vector<Point>& points, bool adopt_sol) {
    for (Point& p : points) {
        g_points.push_back(QtPoint(p));
    }
    init_tree(adopt_sol);
}
void Instance::init_tree(bool adopt_sol) {    
    QtPoint& p0 = g_points[0];
    int minx, maxx, miny, maxy;
    minx = maxx = p0.g_x;
    miny = maxy = p0.g_y;
    for (QtPoint& point : g_points) {
        if (point.g_x < minx) minx = point.g_x;
        if (point.g_y < miny) miny = point.g_y;
        if (point.g_x > maxx) maxx = point.g_x;
        if (point.g_y > maxy) maxy = point.g_y;
    }
    g_qt.init(minx, maxx, miny, maxy);
    if (!adopt_sol){
        init_points();
        return;
    }
    for (QtPoint& point : g_points) {
        if (point.g_exists) {
            point.g_exists = false;
            if (!add(point, std::tuple<int, int>(point.g_solution_x, point.g_solution_y))) {
                printf("Failed to insert point in init tree. Shouldn't happen unless given points weren't feasible.\n");
            }
        }
    }
}
bool Instance::add(QtPoint& p, const std::tuple<int, int>& label_pos) {
    //assert(p == g_points[p.g_id]);
    //assert(&p == &g_points[p.g_id]);

    bool old_ex = p.g_exists;
    
    //std::vector<QtPoint*> xtp = g_qt.get_points();
    //if (old_ex) assert(std::find_if(xtp.begin(), xtp.end(), [&p](const QtPoint* q) { return *q == p; }) != xtp.end());
    
    bool result = g_qt.insert(p, label_pos);
    
    if (result && !old_ex) labeled_points++;

    return result;
}

void Instance::remove(QtPoint& p) {
    if (!p.g_exists) {
        return;
    }
    //assert(g_points[p.g_id] == p);
    //assert(&g_points[p.g_id] == &p);
    //std::vector<QtPoint*> tp = g_qt.get_points();
    //assert(std::find_if(tp.begin(), tp.end(), [&p](const QtPoint* q) { return *q == p; }) != tp.end());
    g_qt.remove(p);

    labeled_points--;
}

bool Instance::check(QtPoint& p, std::tuple<int, int> label_pos) {
    std::vector<Quadtree*> insert_candidates;
    return g_qt.check(p, label_pos, insert_candidates);
}

void Instance::serialize(std::string& file) {
    std::ofstream outfile(file);
    if (outfile.is_open()) {
        outfile << g_points.size() << "\n";
        for (QtPoint& point : g_points) {
            outfile << point.g_x << " " << point.g_y << " " << point.g_width << " " << point.g_height << " "
                    << point.g_name << " " << (int)point.g_exists
                    << " " << point.g_solution_x << " " << point.g_solution_y << "\n";
        }
        outfile.close();
    } else
        std::cout << "Unable to open file" << std::endl;
}

int Instance::getCount() {
    return labeled_points;
}

double Instance::getCoverage() {
    return (double)getCount() / (double)g_points.size();
}

void Instance::init_points() {
    for (QtPoint p : g_points) {
        p.g_exists = false;
        p.g_solution_x = 0;
        p.g_solution_y = 0;
    }
}