//
// Created by lennart on 28.10.2020.
//

#ifndef AE_RULESOLVER_H
#define AE_RULESOLVER_H


#include <vector>
#include <src/util/Point.h>
#include "BaseSolver.h"

class RuleSolver : public BaseSolver {
public:
    std::vector<Point> solve(std::vector<Point> &points) override;
};


#endif //AE_BASICSOLVER_H
