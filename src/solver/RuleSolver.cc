//
// Created by lennart on 28.10.2020.
//

#include "RuleSolver.h"

std::vector<Point> RuleSolver::solve(std::vector<Point>& points)  {
    int points_with_label = 0;

    //alle exists auf false setzen
    for (Point &point : points) {
        point.g_exists = false;
        point.g_solution_x = 0;
        point.g_solution_y = 0;
    }

    //alle exists auf false setzen
    bool problem;
    points_with_label = 0;
    for (std::vector<Point>::size_type i = 0; i < points.size(); i++) {
        Point &point = points[i];
        point.g_exists = true;
        point.g_solution_x = point.g_x;
        point.g_solution_y = point.g_y;

        problem = false;
        for (std::vector<Point>::size_type j = 0; j < points.size(); j++) {
            Point &other = points[j];
            if (other.g_exists && i != j && Point::checkOverlap(point, other)) {
                problem = true;
                break;
            }
        }
        if (problem) {
            point.g_exists = false;
            point.g_solution_x = 0;
            point.g_solution_y = 0;
        } else {
            points_with_label++;
        }
    }

    return points;
}
