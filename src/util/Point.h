//
// Created by lennart on 23.10.20.
//

#ifndef AE_POINT_H
#define AE_POINT_H


#include <utility>
#include <vector>
#include <string>
#include <fstream>
#include <iostream>

class Point {
public:
    enum Feasibility {
        OK, CORNER_ERROR, OVERLAP_ERROR
    };

    unsigned long long g_id;

    int g_x, g_y;
    int g_width, g_height;
    std::string g_name;

    bool g_exists;

    int g_solution_x, g_solution_y;

    Point(unsigned long long id, int x, int y, int l, int h, std::string t, bool b_, int x_, int y_) {
        g_id = id;

        g_x = x;
        g_y = y;

        g_height = h;
        g_width = l;

        g_name = std::move(t);

        g_exists = b_;
        g_solution_x = x_;
        g_solution_y = y_;
    }

    static std::vector<Point> parseFile(std::string &file);

    static bool checkOverlap(Point point, std::vector<Point> &points);

    static bool checkOverlap(Point &p1, Point &p2);

    static void serializeFile(std::string &outFile, std::vector<Point> &points);

    std::vector<std::tuple<int, int>> getPossibleLabelPositions();

    static Point::Feasibility checkFeasibility(std::vector<Point> points, bool verbose, bool onlyErrors);

    bool operator==(Point const& point) const { return point.g_id == g_id; }
    bool operator!=(Point const& point) const { return point.g_id != g_id; }

    static bool checkOverlapEverPossible(const Point &p1, const Point &p2);

    static bool checkOverlapEverPossible(Point point, std::vector<Point> &points);
};


#endif //AE_POINT_H
