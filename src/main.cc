#include <src/solver/BasicSolver.h>
#include <src/solver/BetterBasicSolver.h>
#include <src/solver/GeneticAlgorithm.h>
#include <src/solver/HeuristicSolver1.h>
#include <src/solver/OptimalSolver.h>
#include <src/solver/QtGeneticAlgorithm.h>
#include <src/solver/QtSimulatedAnnealingSolver.h>
#include <src/solver/RandomSolver.h>
#include <src/solver/RuleSolver.h>
#include <src/solver/SimulatedAnnealingSolver.h>

#include <algorithm>
#include <chrono>
#include <fstream>
#include <functional>
#include <iomanip>
#include <iostream>
#include <tuple>
#include <vector>

#include "src/util/Point.h"

int main(int argc, char *argv[]) {
    //Parse arguments

    std::string in;
    std::string out;
    std::string eval;

    std::string solver = "basic";

    bool verbose = false;
    double repetitions = 1;
    bool evaluateSolvers = false;
    bool preprocessing = false;
    int iterations = -1;  // for qtSimAnn and qtGenAlg
    for (int i = 1; i < argc; ++i) {
        if (std::string(argv[i]) == "-eval") {
            if (i + 1 < argc) {
                eval = argv[++i];
            } else {
                std::cerr << "-eval option requires one argument." << std::endl;
                return 1;
            }
        } else if (std::string(argv[i]) == "-in") {
            if (i + 1 < argc) {
                in = argv[++i];
            } else {
                std::cerr << "-in option requires one argument." << std::endl;
                return 1;
            }
        } else if (std::string(argv[i]) == "-out") {
            if (i + 1 < argc) {
                out = argv[++i];
            } else {
                std::cerr << "-out option requires one argument." << std::endl;
                return 1;
            }
        } else if (std::string(argv[i]) == "-solver") {
            if (i + 1 < argc) {
                solver = argv[++i];
            } else {
                std::cerr << "-solver option requires one argument." << std::endl;
                return 1;
            }
        } else if (std::string(argv[i]) == "-r" || std::string(argv[i]) == "-repetitions") {
            if (i + 1 < argc) {
                repetitions = std::stod(argv[++i]);  // TODO check if positive INTEGER only for division double for now
            } else {
                std::cerr << "-r option requires one argument." << std::endl;
                return 1;
            }
        } else if (std::string(argv[i]) == "-i" || std::string(argv[i]) == "-iterations") {
            if (i + 1 < argc) {
                iterations = std::stod(argv[++i]);  // TODO check if positive INTEGER only for division double for now
            } else {
                std::cerr << "-r option requires one argument." << std::endl;
                return 1;
            }
        } else if (std::string(argv[i]) == "-verbose" || std::string(argv[i]) == "-v") {
            verbose = true;
        } else if (std::string(argv[i]) == "-preprocessing" || std::string(argv[i]) == "-p") {
            preprocessing = true;
        } else if (std::string(argv[i]) == "-evalSolvers") {
            evaluateSolvers = true;
        } else {
            std::cerr << "Unrecognized option: " << std::string(argv[i]) << std::endl;
        }
    }

    if (eval.length() > 0) {
        std::vector<Point> points = Point::parseFile(eval);
        //check if label is on corner

        Point::Feasibility feasibility = Point::checkFeasibility(points, false, false);

        if (feasibility == Point::Feasibility::CORNER_ERROR) {
            return 1;
        } else if (feasibility == Point::Feasibility::OVERLAP_ERROR) {
            return 1;
        } else if (feasibility == Point::Feasibility::OK) {
            return 0;
        }
    } else if (in.length() > 0 && out.length() > 0) {
        std::vector<Point> points_in = Point::parseFile(in);
        std::vector<Point> points;
        std::vector<Point> points_done;

        std::vector<std::string> toEvaluate{};
        std::ofstream file;
        file.open(out);

        if (evaluateSolvers == true) {
            toEvaluate = {"SimAnn", "heur1", "betterbasic"};
            file << "\tsolved\t"
                 << "time\n";

        } else {
            //just execute the passed solver
            toEvaluate = {solver};
        }

        // fixed strange seg fault like Lennart did on master
        /* TODO: maybe we should abandon 'toEvaluate' and 'repetions'
           and do all this stuff in auto_eval.py. 
           Else we need to consider that non-deterministic algos produce
           different solutions on every execution. */

        std::vector<Point> points_best;
        auto start = std::chrono::high_resolution_clock::now();
        //for (int p = 0; p < toEvaluate.size(); p++) {
        //**********************************
        for (int x = 0; x < repetitions; x++) {
            BaseSolver *bs;
            if (solver == "basic") {
                BasicSolver s;
                bs = &s;
            } else if (solver == "betterbasic") {
                BetterBasicSolver s;
                bs = &s;
            } else if (solver == "opt") {
                OptimalSolver s;
                bs = &s;
            } else if (solver == "heur1") {
                HeuristicSolver1 s;
                bs = &s;
            } else if (solver == "SimAnn") {
                SimulatedAnnealingSolver s;
                bs = &s;
            } else if (solver == "qtSimAnn") {
                QtSimulatedAnnealingSolver s(iterations);
                bs = &s;
            } else if (solver == "rnd") {
                RandomSolver s;
                bs = &s;
            } else if (solver == "rule") {
                RuleSolver s;
                bs = &s;
            } else if (solver == "GenAlg") {
                GeneticAlgorithm s;
                bs = &s;
            } else if (solver == "qtGenAlg") {
                QtGeneticAlgorithm s(iterations);
                bs = &s;

            } else {
                //TODO maybe print error
                std::cout << "Couldn't find the given solver. Use basic solver instead.\n"
                          << std::endl;
                BasicSolver s;
                bs = &s;
            }

            if(preprocessing) {
                int cou = 0;
                for (auto &pointIn : points_in) {
                    if (!Point::checkOverlapEverPossible(pointIn, points_in)) {
                        cou++;
                        pointIn.g_solution_x = pointIn.g_x;
                        pointIn.g_solution_y = pointIn.g_y;
                        pointIn.g_exists = true;
                        points_done.push_back(pointIn);
                    } else {
                        points.push_back(pointIn);
                    }
                }
            }else{
                points = points_in;
            }
            points_best = bs->solve(points);

            for (const auto& s : points_done){
                points_best.push_back(s);
            }
        }
        //**********************************

        auto end = std::chrono::high_resolution_clock::now();
        auto milliseconds = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
        std::time_t end_time = std::chrono::high_resolution_clock::to_time_t(end);

        //Check count of data points
        auto points_with_label = 0;
        for (auto &&p : points_best)
            if (p.g_exists)
                ++points_with_label;

        //Should only be for debugging purposes
        //check if solution is even legal

        // 'Point::checkFeasibility' takes too much time on large instances (e.g. 100.000)
        //if (Point::checkFeasibility(points_best, false, true) > 0) {
        //    std::cerr << "Created solution is not feasible!" << std::endl;
        //    return 1;
        //}

        // creates an instance from the solution. This automatically checks
        // feasability of the solution. If there's a problem, something will
        // be printed to std::out.
        Instance inst(points_best, true);
        // only for debugging. Could be slow.
        assert(inst.check_consistency());

        if (verbose) {
            std::cout << "finished computation at " << std::ctime(&end_time)
                      << "elapsed time: " << milliseconds.count() << "ms\n"
                      << "Points with label: " << points_with_label << "/" << points.size() << std::endl;
        }

        std::cout << points_with_label << "\t" << std::fixed << std::setprecision(2)
                  << ((milliseconds.count() / 1000.0) / repetitions)
                  << std::endl;

        //Write to output file
        /*if (evaluateSolvers == true) {
            file << toEvaluate[p] << "\t" << points_with_label << "\t" << (milliseconds.count() / 1000.0);

            file << "\n";
            //file.close();

        } else {
            Point::serializeFile(out, points_best);
        }*/
        Point::serializeFile(out, points_best);

        //}
    } else {
        // TODO: replace 'progname' with actual program name
        std::cerr << "No options passed! \n\nprogname -in dat1 -out dat2 [-solver name] [(-i|-iterations) iterations (for qtSimAnn)] [(-r|-repetitions) number] [-v|-verbose]\nprogname -eval dat1" << std::endl;
        return 1;
    }
    return 0;
}
